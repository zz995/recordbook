$(function () {
    'use strict';

    $('body').on('click', '.name-surname', function (e) {
        var $this = $(this);
        var contactId = $this.data('id');

        var header =
            '<div class="uk-modal-header">'
            + '<span class="uk-h2">' + 'Информация о контакте' + '</span>'
            + '</div>';

        var modalTmpId = 'modalWindowTmp' + ~~(Math.random() * 100000);
        var modalTmpHtml =
            '<div id="' + modalTmpId + '" class="uk-modal">'
                + '<div class="uk-modal-dialog">'
                    + '<button type="button" class="uk-modal-close uk-close"></button>'
                    + header
                    + '<span autofocus></span>'
                    + '<div class="modal-content">'
                        + '<div class="uk-text-center">'
                            + '<i class="uk-icon-spinner uk-icon-spin uk-icon-medium"></i>'
                        + '</div>'
                    + '</div>'
                + '</div>'
            + '</div>';
        $("body").append(modalTmpHtml);

        UIkit.modal('#' + modalTmpId).show();
        $('#' + modalTmpId).one({
            'hide.uk.modal': function() {
                $(this).remove();
            }
        });

        $.ajax({'url': '/contact/ajaxInfo?contact=' + contactId}).done(function (responseJson) {
            $('#' + modalTmpId).find('.modal-content').empty().append(responseJson.html);
        }).fail(function (jqXHR) {
            $('#' + modalTmpId).find('.modal-content').empty().append('<span>Произошла ошибка</span>');
        });

        return false;
    });

    $('body').on('click', '.column-sort, .pages a', function (e) {
        var $this = $(this);
        var href = $this.data('href');
        var contentElem = $(".contacts");

        contentElem.addClass('contacts_wait-load');
        if ($(window).scrollTop() > contentElem.offset().top) {
            UIkit.Utils.scrollToElement(UIkit.$(contentElem, { /* options */ }));
        }

        $.ajax({'url': href}).done(function (responseJson) {
            contentElem.empty().append(responseJson.html);
        }).always(function (jqXHR) {
            contentElem.removeClass('contacts_wait-load');
        });

        return false;
    });
});