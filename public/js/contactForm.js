$(function () {
   'use strict';

    $("[name='country_id']").on('change', function (e) {
        var $this = $(this);
        var value = $this.val();
        var cityElem = $("[name='city_id']");
        if (value === '') {
            cityElem.val('').prop('disabled', true);
        } else {
            $.ajax({
                'url': '/city/ajaxCityByCountry?country=' +  value 
            }).done(function (responseJson) {
                cityElem.empty();
                var html = '<option value="">-- Выберите город --</option>';
                responseJson.cities.forEach(function (item) {
                    html += '<option value="' + item.id + '">' + item.name + '</option>';
                });
                cityElem.append(html);
            });
            cityElem.prop('disabled', false);
        }
    });

    $("body").on('change', '.network__id', function (e) {
        var $this = $(this);
        var value = $this.val();
        var networksContentElem = $(".networks-content");
        var networkElem = $this.closest('.network');
        var networkExemElem = $('.network_example');
        var networkTitleElem = networkElem.find(".network__title");
        var networkLinkElem = networkElem.find(".network__link");
        var networkIdElem = networkElem.find(".network__id");
        var networkDelElem = networkElem.find(".network-delete");

        if (value == -1) {
            networkIdElem.addClass('uk-hidden');
            networkTitleElem.removeClass('uk-hidden');
            networkTitleElem.focus();
        }

        if (value != '') {
            if (networkElem.hasClass('network_new')) {
                networkElem.removeClass('network_new');
                networksContentElem.append(networkExemElem.clone(true).removeClass('uk-hidden network_example'));
            }
            networkDelElem.removeClass('uk-hidden');
        }
    });

    $("body").on('blur', '.network__title', function (e) {
        var $this = $(this);
        var value = $this.val();
        var networkElem = $this.closest('.network');
        var networkTitleElem = networkElem.find(".network__title");
        var networkLinkElem = networkElem.find(".network__link");
        var networkIdElem = networkElem.find(".network__id");

        if (value == '') {
            networkIdElem.val('');
            networkIdElem.removeClass('uk-hidden');
            networkTitleElem.addClass('uk-hidden');
        }
    });

    $("body").on('click', '.network-delete', function (e) {
        var $this = $(this);
        var networkElem = $this.closest('.network');
        networkElem.remove();
    });
});