<?php

use Phalcon\Mvc\Application;

try {
    $config = include __DIR__ . "/../app/config/config.php";
    if (isset($config->application->env) && $config->application->env == 'debug') {
        error_reporting(E_ALL);
        function errHandle($errNo, $errStr, $errFile, $errLine) {
            if ( error_reporting() == 0 ) {
                return;
            }
            $msg = "$errStr in $errFile on line $errLine";
            $msg .= '';
        }
        set_error_handler('errHandle');
    } else {
        error_reporting(E_ALL & ~E_NOTICE);
    }

    include __DIR__ . "/../app/config/loader.php";
    include __DIR__ . "/../app/config/services.php";

    $application = new Application($di);
    echo $application->handle()->getContent();
} catch (\Exception $err) {
    error_log(
        print_r(
            $err->getMessage(), true
        )
    );

    http_response_code('500');
    echo "<div style='text-align: center'><h2>500</h2><h2>Ошибка сервера</h2></div>";
} catch (\Error $err) {
    error_log(
        print_r(
            $err->getMessage() . ' in ' . $err->getFile() . ' on line ' . $err->getLine(), true
        )
    );

    http_response_code('400');
    echo "<div style='text-align: center'><h2>400</h2><h2>Неправильный запрос</h2></div>";
}