<?php

namespace MyApp\Controllers;

use MyApp\Forms\CityForm;
use MyApp\Models\City;

class CityController extends ControllerBase
{
    public function initialize() {

    }

    public function indexAction() {
        $countryRef = $this->countryModelRef;
        $cityRef = $this->cityModelRef;

        $this->tag->setTitle('Список городов');
        $query = City::query();
        $query->join(
            $countryRef,
            "$countryRef.id = $cityRef.country_id"
        );
        $cities = $query->orderBy("$cityRef.name ASC")->execute();
        $this->view->cities = $cities;
        $this->view->navbar = 'cities';
    }

    public function formAction() {
        $ref = '/city';
        $method = $this->getMethod();
        $cityId = $this->dispatcher->getParam(0, 'emptytonull', null);
        $this->tag->setTitle(($cityId ? 'Редактирование' : 'Добавление') . ' города');
        $postData = [];
        if ($method == 'POST') {
            $postData = $this->request->getPost();
        }
        $form = new CityForm();

        if (isset($cityId)) {
            $city = City::findFirst($cityId);
        } else {
            $city = new City();
        }

        if ($method == 'DELETE') {
            if ($city->delete()) {
                $this->flashSession->success('Данные успешно удалены');
                return $this->response->redirect($ref);
            };
            $this->flash->error($city->getMessages());
        } elseif ($method == 'POST') {
            $data = (object)[];
            $form->bind($postData, $data);
            if ($form->isValid()) {
                $city->name = $data->name;
                $city->country_id = $data->country_id;
                if ($city->save()) {
                    $this->flashSession->success(
                        isset($cityId)
                            ? 'Данные города успешно изменены'
                            : 'Город успешно создан'
                    );
                    return $this->response->redirect($ref);
                }

                $this->flash->error($city->getMessages());
            }
        } else {
            $form->setEntity((object)$city);
        }

        $this->view->query = [
            'cityId' => $cityId
        ];
        $this->view->ref = $ref;
        $this->view->form = $form;
    }

    public function ajaxCityByCountryAction() {
        $countryId = $this->request->get('country', ['emptytonull'], null);
        if (is_null($countryId)) {
            return $this->routeBadParam();
        }

        $cities = City::find(
            [
                "country_id = :id:",
                "bind" => [
                    "id" => $countryId
                ]
            ]
        );

        $this->response->setJsonContent(
            ['cities' => $cities]
        );
        return $this->response->send();
    }
}