<?php

namespace MyApp\Controllers;

use MyApp\Forms\CountryForm;
use MyApp\Models\Country;

class CountryController extends ControllerBase
{
    public function initialize() {

    }

    public function indexAction() {
        $this->tag->setTitle('Список стран');
        $query = Country::query();
        $countries = $query->orderBy('name ASC')->execute();
        $this->view->countries = $countries;
        $this->view->navbar = 'countries';
    }

    public function formAction() {
        $ref = '/country';
        $method = $this->getMethod();
        $countryId = $this->dispatcher->getParam(0, 'emptytonull', null);
        $this->tag->setTitle(($countryId ? 'Редактирование' : 'Добавление') . ' страны');
        $postData = [];
        if ($method == 'POST') {
            $postData = $this->request->getPost();
        }
        $form = new CountryForm();

        if (isset($countryId)) {
            $country = Country::findFirst($countryId);
        } else {
            $country = new Country();
        }
        $countBindCities = $country->cities->count();

        if ($method == 'DELETE') {
            if ($countBindCities == 0 && $country->delete()) {
                $this->flashSession->success('Данные успешно удалены');
                return $this->response->redirect($ref);
            }
            $this->flash->error($country->getMessages());
            if ($countBindCities == 0) {
                $this->flashSession->success('Невозможно удалить');
            }
        } elseif ($method == 'POST') {
            $data = (object)[];
            $form->bind($postData, $data);
            if ($form->isValid()) {
                $country->name = $data->name;

                if ($country->save()) {
                    $this->flashSession->success(
                        isset($countryId)
                            ? 'Данные страны успешно изменены'
                            : 'Страна успешно создана'
                    );
                    return $this->response->redirect($ref);
                }

                $this->flash->error($country->getMessages());
            }
        } else {
            $form->setEntity((object)$country);
        }

        $this->view->data = [
            'countBindCities' => @$countBindCities ?: 0
        ];
        $this->view->query = [
            'countryId' => $countryId
        ];
        $this->view->ref = $ref;
        $this->view->form = $form;
    }
}