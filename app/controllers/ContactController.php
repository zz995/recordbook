<?php

namespace MyApp\Controllers;

use MyApp\Forms\ContactForm;
use MyApp\Library\ContactNetworks;
use MyApp\Models\Contact;
use MyApp\Models\Network;

class ContactController extends ControllerBase
{
    public function initialize() {
    }

    public function indexAction() {
        $this->tag->setTitle('Записная книжка');

        $page = 1;
        $limit = 10;
        $query = Contact::query();
        $queryCount = $query;
        $count = $queryCount->execute()->count();
        $masPage = ceil($count / $limit);
        $contacts = $query->orderBy("name ASC, surname ASC")->limit($limit, 0)->execute();

        $this->view->contacts = $contacts;
        $this->view->query = [
            'page' => $page,
            'limit' => $limit
        ];
        $this->view->data = [
            'maxPage' => $masPage,
            'fieldSort' => 'name',
            'directSort' => 'asc'
        ];
        $this->view->navbar = 'addressBook';
    }

    public function formAction() {
        $ref = '/';
        $method = $this->getMethod();
        $contactId = $this->dispatcher->getParam(0, 'emptytonull', null);
        $this->tag->setTitle(($contactId ? 'Редактирование' : 'Добавление') . ' записи');
        $postData = [];
        if ($method == 'POST') {
            $postData = $this->request->getPost();
        }
        $networks = new ContactNetworks(Network::find());

        if (isset($contactId)) {
            $contact = Contact::findFirst($contactId);
            if (!isset($contact->id)) {
                return $this->routeNotFound();
            }
        } else {
            $contact = new Contact();
        }

        if ($method == 'DELETE') {
            $contact->unlinkImage();
            if ($contact->delete()) {
                $this->flashSession->success('Данные успешно удалены');
                return $this->response->redirect($ref);
            };
            $this->flash->error($contact->getMessages());
        } elseif ($method == 'POST') {
            $data = (object)[];
            $form = new ContactForm(null, ['country' => @$postData['country_id'] ]);
            $form->bind($postData, $data);

            $networks->setNetworks(@$postData['network']);

            if ($networks->isValid() && $form->isValid() && $form->validFiles()) {

                $contact->surname = $data->surname;
                $contact->name = $data->name;
                $contact->patronymic = $data->patronymic;
                $contact->birth = $data->birth;
                $contact->phone = $data->phone;
                $contact->email = $data->email;
                $contact->city_id = $data->city_id;

                $files = $form->getFiles('image');
                if (!empty($files) && count($files) > 0) {
                    $contact->loadImage($files);
                }

                if ($contact->save() && $networks->save($contact)) {
                    $this->flashSession->success(
                        isset($contactId)
                            ? 'Данные записи успешно изменены'
                            : 'Запись успешно создана'
                    );
                    return $this->response->redirect($ref);
                }

                $this->flash->error($contact->getMessages());
            }
        } else {
            $contactData = (object)$contact;
            $contactData->country_id = @$contact->city->country_id;
            if (isset($contact->birth)) {
                $contactData->birth = date('Y-m-d', strtotime($contact->birth));
            }

            $form = new ContactForm($contactData, ['country' => $contactData->country_id ]);
            $networks->setContactNetworks($contact);
        }

        $this->view->query = [
            'contactId' => $contactId
        ];
        $this->view->ref = $ref;
        $this->view->form = $form;
        $this->view->existNetworks = Network::find();
        $this->view->networks = $networks->getNetworks();
    }

    public function ajaxTableAction() {
        $page = $this->request->getQuery('page', 'page', 1);
        $limit = $this->request->getQuery('limit', 'limit', 10);
        $sort = $this->request->getQuery('sort', 'emptytonull', null);
        @list($fieldSort, $directSort) = explode('_', $sort ?: '');
        if (!in_array($fieldSort, ['name', 'phone', 'email', 'birth'])) {
            $fieldSort = 'name';
        }
        if (!in_array($directSort, ['asc', 'desc'])) {
            $directSort = 'asc';
        }

        $query = Contact::query();
        $queryCount = $query;
        $count = $queryCount->execute()->count();
        $masPage = ceil($count / $limit);
        $page = $page > $masPage ? $masPage : $page;
        $offset = ($page - 1) * $limit;

        if ($fieldSort == 'name') {
            $query->orderBy("name $directSort, surname $directSort, id DESC");
        } else {
            $query->orderBy("$fieldSort $directSort, id DESC");
        }

        $contacts = $query->limit($limit, $offset)->execute();

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        $this->view->contacts = $contacts;
        $this->view->query = [
            'page' => $page,
            'limit' => $limit
        ];
        $this->view->data = [
            'maxPage' => $masPage,
            'fieldSort' => $fieldSort,
            'directSort' => $directSort
        ];

        $html = $this->view->render('ajax', 'contactTable')->getContent();
        $this->response->setJsonContent(
            ['html' => $html]
        );

        return $this->response->send();
    }

    public function ajaxInfoAction() {
        $contactId = $this->request->get('contact', ['emptytonull'], null);
        if (is_null($contactId)) {
            return $this->routeBadParam();
        }

        $contact = Contact::findFirst($contactId);
        if (!isset($contact->id)) {
            return $this->routeBadParam();
        }

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        $this->view->contact = $contact;

        $html = $this->view->render('ajax', 'contactInfo')->getContent();
        $this->response->setJsonContent(
            ['html' => $html]
        );

        return $this->response->send();
    }
}