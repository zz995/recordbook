<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    array(
        'MyApp\Controllers' => __DIR__ . '/../controllers/',
        'MyApp\Forms' => __DIR__ . '/../forms/',
        'MyApp\Models' => __DIR__ . '/../models/',
        'MyApp\Plugins' => __DIR__ . '/../plugins/',
        'MyApp\Tag' => __DIR__ . '/../tag/',
        'MyApp\Library' => __DIR__ . '/../library/',
    )
)->register();