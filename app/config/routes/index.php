<?php

use Phalcon\Mvc\Router;

$router = new Router();

$router->addGet('/contact', array(
    'namespace' => 'MyApp\Controllers',
    'controller' => 'error',
    'action' => 'notFound'
));

$router->addGet('/contact/index', array(
    'namespace' => 'MyApp\Controllers',
    'controller' => 'error',
    'action' => 'notFound'
));

$router->addGet('/', array(
    'namespace' => 'MyApp\Controllers',
    'controller' => 'contact',
    'action' => 'index'
));

$router->notFound(array(
    'namespace' => 'MyApp\Controllers',
    'controller' => 'error',
    'action' => 'notFound'
));
return $router;