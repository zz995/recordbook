<?php

namespace MyApp\Plugins;

use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use \Exception;

class NotFoundPlugin extends Plugin
{
    public function beforeException(Event $event, Dispatcher $dispatcher, Exception $exception)
    {
        if ($event->getType() == 'beforeException') {
            switch ($exception->getCode()) {
                case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $action = 'notFound';
                    break;
                default:
                    $action = 'index';
                    break;
            }

            $dispatcher->forward(array(
                'namespace' => 'MyApp\Controllers',
                'controller' => 'error',
                'action' => $action,
                'params' => array('err' => $exception)
            ));

            return false;
        }

        return true;
    }
}