<?php

namespace MyApp\Models;

use Phalcon\Mvc\Model;

class City extends Model
{
    public $id;
    public $name;

    public function initialize()
    {
        $this->hasMany(
            "id",
            "MyApp\\Models\\Contact",
            "city_id",
            array(
                'alias' => 'contact'
            )
        );

        $this->belongsTo(
            "country_id",
            "MyApp\\Models\\Country",
            "id",
            array(
                'alias' => 'country'
            )
        );
    }
}