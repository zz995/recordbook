<?php

namespace MyApp\Models;

use Phalcon\Mvc\Model;

class Link extends Model
{
    public $id;
    public $url;
    public $network_id;

    public function initialize()
    {
        $this->belongsTo(
            "network_id",
            "MyApp\\Models\\Network",
            "id",
            array(
                'alias' => 'networks'
            )
        );

        $this->belongsTo(
            "contact_id",
            "MyApp\\Models\\Contact",
            "id",
            array(
                'alias' => 'contact'
            )
        );
    }
}