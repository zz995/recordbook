<?php

namespace MyApp\Models;

use Phalcon\Image\Adapter\Gd;
use Phalcon\Mvc\Model;

class Contact extends Model
{
    public $id;
    public $surname;
    public $name;
    public $patronymic;
    public $phone;
    public $email;
    public $birth;
    public $photo;

    public function initialize()
    {
        $this->belongsTo(
            "city_id",
            "MyApp\\Models\\City",
            "id",
            array(
                'alias' => 'city'
            )
        );

        $this->hasMany(
            "id",
            "MyApp\\Models\\Link",
            "contact_id",
            array(
                'alias' => 'links'
            )
        );
    }

    public function unlinkImage() {
        $prePath = realpath(__DIR__ . '/../../public');
        if (!empty($this->photo)) {
            unlink($prePath . $this->photo);
        }
    }

    public function loadImage($files) {
        $prePath = realpath(__DIR__ . '/../../public');
        $path = '/img/uploads/';
        $size = 600;
        $file = $files[0];
        $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);

        if (!empty($this->photo)) {
            unlink($prePath . $this->photo);
        }

        $name = '';
        for ($i = 0; $i < 1000; $i++) {
            $md5 = md5(uniqid(rand(), true));
            $name = $md5 . '.' . $ext;

            if (!file_exists($prePath . $path . $name)) {
                break;
            }
        }

        if ($name == '') {
            throw new \Exception('Неудалось подобрать уникального имени');
        }

        $imageGD = new GD($file->getTempName());
        $imageWidth = $imageGD->getWidth();

        if ($imageWidth > $size) {
            $imageGD->resize(
                $size,
                null,
                \Phalcon\Image::WIDTH
            );
        }

        $imageGD->save($prePath . $path . $name);
        unset($imageGD);

        $this->photo = $path . $name;
    }
}