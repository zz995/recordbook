<?php

namespace MyApp\Models;

use Phalcon\Mvc\Model;

class Network extends Model
{
    public $id;
    public $name;
    public $url;

    public function initialize()
    {
        $this->hasMany(
            "id",
            "MyApp\\Models\\Link",
            "network_id",
            array(
                'alias' => 'link'
            )
        );
    }
}