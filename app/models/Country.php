<?php

namespace MyApp\Models;

use Phalcon\Mvc\Model;

class Country extends Model
{
    public $id;
    public $name;

    public function initialize()
    {
        $this->hasMany(
            "id",
            "MyApp\\Models\\City",
            "country_id",
            array(
                'alias' => 'cities'
            )
        );
    }
}