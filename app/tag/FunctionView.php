<?php

namespace MyApp\Tag;

use Phalcon\Tag;

class FunctionView extends Tag {
    static public function printTitle() {
        $title = \Phalcon\Tag::getTitle(false);
        return "<title>$title</title>";
    }

    static public function dateFormat($date, $format = 'd.m.Y') {
        return date($format, strtotime($date));
    }

    static public function updateQueryStringParameter(string $uri, string $key, $value): string {
        $value = (string)$value;
        preg_match("/(.*[?&])$key=.*?(&.*|$)/i", $uri, $matches);
        $querySep = strpos($uri, "?") === false ? '?' : '&';
        if (count($matches) > 0) {
            return "{$matches[1]}$key=$value{$matches[2]}";
        } else {
            return $uri . $querySep . "$key=$value";
        }
    }

    static public function uri() {
        if (!isset($_SERVER['REQUEST_URI'])) return '';
        return $_SERVER['REQUEST_URI'];
    }
}