{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}

    {%- macro printTableRow(city, isHead) %}
        <div class="uk-grid">
            <div class="uk-width-2-10 uk-width-medium-1-10">
                {{ city['n'] }}
            </div>
            <div class="uk-width-4-10 uk-width-medium-3-10">
                {{ city['name'] }}
            </div>
            <div class="uk-width-4-10 uk-width-medium-3-10">
                {{ city['country'] }}
            </div>
            <div class="uk-visible-small uk-margin-small-top"></div>
            <div class="uk-width-1-1 uk-width-medium-3-10">
                {% if isHead %}
                    {{ city['edit'] }}
                {% else %}
                    <a href="/city/form/{{ city['n'] }}">
                        {{ city['edit'] }}
                    </a>
                {% endif %}
            </div>
        </div>
    {%- endmacro %}

    <div class="uk-margin-large">
        <div class="uk-container uk-container-center container">
            <div class="uk-grid uk-mar">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
            </div>

            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <a href="/city/form">
                        <i class="uk-icon-plus uk-icon-medium uk-display-inline"></i>&nbsp;Добавить город
                    </a>
                </div>
                <div class="uk-width-1-1">
                    <hr>
                </div>
            </div>

            <div class="uk-grid">
                {% if cities.count() is empty %}
                    <div class="uk-width-1-1">
                        Ничего не найдено
                    </div>
                {% else %}
                    <div class="uk-width-1-1 cities table">
                        <div class="table__head">
                            {{ printTableRow(['n': '№', 'name': 'Название города', 'country': 'Название страны', 'edit': ''], true) }}
                        </div>
                        {% for city in cities %}
                            <div class="table__main">
                                {{ printTableRow(['n': city.id, 'name': city.name, 'country': city.country.name, 'edit': 'Редактировать'], false) }}
                            </div>
                        {% endfor %}
                    </div>
                {% endif %}
            </div>

        </div>
    </div>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}