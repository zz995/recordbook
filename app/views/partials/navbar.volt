<div class="">
    <nav class="uk-navbar">
        <div class="uk-container uk-container-center">
            <ul class="uk-navbar-nav">
                <li class="{{ navbar == 'addressBook' ? 'uk-active' : '' }}">
                    <a href="/">Записная книжка</a>
                </li>
                <li class="{{ navbar == 'countries' ? 'uk-active' : '' }}">
                    <a href="/country">Страны</a>
                </li>
                <li class="{{ navbar == 'cities' ? 'uk-active' : '' }}">
                    <a href="/city">Города</a>
                </li>
            </ul>
        </div>
    </nav>
</div>