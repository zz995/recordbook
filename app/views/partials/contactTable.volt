{{ partial("partials/pages") }}

{%- macro printFieldSort(title, field, fieldSort, directSort, url) %}
    <span
            class="column-sort {{ fieldSort == field ? 'sort-' ~ directSort : '' }}"
            data-href="{{ tag.updateQueryStringParameter(url, 'sort', field ~ (directSort == 'desc' ? '_asc' : '_desc')) }}"
    >
        {{ title }}
    </span>
{%- endmacro %}

{%- macro printTableRow(contact, isHead, fieldSort, directSort, url) %}
        <div class="uk-grid">
            <div class="uk-width-5-10 uk-width-medium-3-10">
                {% if isHead %}
                    {{ printFieldSort(contact['name'], 'name', fieldSort, directSort, url) }}
                {% else %}
                    <a href="#" data-id="{{ contact['n'] }}" class="name-surname">
                        {{ contact['name'] }}
                    </a>
                {% endif %}
            </div>
            <div class="uk-width-5-10 uk-width-medium-2-10">
                {% if isHead %}
                    {{ printFieldSort(contact['phone'], 'phone', fieldSort, directSort, url) }}
                {% else %}
                    {{ contact['phone'] }}
                {% endif %}
            </div>
            <div class="uk-width-5-10 uk-width-medium-2-10">
                {% if isHead %}
                    {{ printFieldSort(contact['email'], 'email', fieldSort, directSort, url) }}
                {% else %}
                    {{ contact['email'] }}
                {% endif %}
            </div>
            <div class="uk-width-5-10 uk-width-medium-1-10">
                {% if isHead %}
                    {{ printFieldSort(contact['birth'], 'birth', fieldSort, directSort, url) }}
                {% else %}
                    {{ contact['birth'] }}
                {% endif %}
            </div>
            <div class="uk-visible-small uk-margin-small-top"></div>
            <div class="uk-width-1-1 uk-width-medium-2-10">
                {% if isHead %}
                    {{ contact['edit'] }}
                {% else %}
                    <a href="/contact/form/{{ contact['n'] }}">
                        {{ contact['edit'] }}
                    </a>
                {% endif %}
            </div>
        </div>
{%- endmacro %}

{% if contacts.count() is empty %}
    <div class="uk-width-1-1">
        Ничего не найдено
    </div>
{% else %}
    <div class="uk-width-1-1 table">
        <div class="table__head">
            {{ printTableRow([
            'n': '№',
            'name': 'Имя Фамилия',
            'phone': 'Номер телефона',
            'email': 'Электронная почта',
            'birth': 'Дата рождения',
            'edit': ''
            ], true, data['fieldSort'], data['directSort'], tag.updateQueryStringParameter(url, 'page', 1)) }}
        </div>
        {% for contact in contacts %}
            <div class="table__main">
                {{ printTableRow([
                'n': contact.id,
                'name': contact.name ~ ' ' ~ contact.surname,
                'phone': contact.phone,
                'email': contact.email,
                'birth': contact.birth is defined ? tag.dateFormat(contact.birth) : '-',
                'edit': 'Редактировать'
                ], false, data['fieldSort'], data['directSort'], tag.updateQueryStringParameter(url, 'page', 1)) }}
            </div>
        {% endfor %}
    </div>
    <div class="uk-width-1-1 uk-margin-top uk-container-center pages">
        {{ printPages(query['page'], 7, data['maxPage'], url) }}
    </div>
{% endif %}