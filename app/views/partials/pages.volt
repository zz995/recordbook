
{%- macro printPages(page, steps, maxPage, url) %}
    {% set min = page - steps %}
    {% set offset = 0 %}
    {% if min < 1 %}
        {% set offset = min - 1 %}
        {% set min = 1 %}
    {% endif %}

    {% set max = page + steps - offset %}
    {% set offset = 0 %}
    {% if max > maxPage %}
        {% set offset = max - maxPage %}
        {% set max = maxPage %}
    {% endif %}

    {% set min = min - offset %}
    {% if min < 1 %}
        {% set min = 1 %}
    {% endif %}

    {% if max < min %}
        {% set max = min %}
    {% endif %}

    <ul class="uk-pagination">

        {% if page > 1 %}
            <li>
                <a data-href="{{ tag.updateQueryStringParameter(url, 'page', 1) }}" href="#"><i class="uk-icon-angle-double-left"></i></a>
            </li>
            <li>
                <a data-href="{{ tag.updateQueryStringParameter(url, 'page', +page - 1) }}" href="#"><i class="uk-icon-angle-left"></i></a>
            </li>
        {% endif %}

        {% for num in min .. max %}
            {% if num is page %}
                <li class="uk-active">
                    <span>{{ num }}</span>
                </li>
            {% else %}
                <li>
                    <a data-href="{{ tag.updateQueryStringParameter(url, 'page', num) }}" href="#">{{ num }}</a>
                </li>
            {% endif %}
        {% endfor %}

        {% if page < maxPage %}
            <li>
                <a data-href="{{ tag.updateQueryStringParameter(url, 'page', +page + 1) }}" href="#"><i class="uk-icon-angle-right"></i></a>
            </li>
            <li>
                <a data-href="{{ tag.updateQueryStringParameter(url, 'page', maxPage) }}" href="#"><i class="uk-icon-angle-double-right"></i></a>
            </li>
        {% endif %}

    </ul>
{%- endmacro %}