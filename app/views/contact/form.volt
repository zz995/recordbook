{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}
    {%- macro printNetwork(id, title, link, msg, existNetworks, isNew, isExample) %}

        <div class="network uk-grid {{ isExample ? 'uk-hidden network_example' : '' }} {{ isNew ? 'network_new' : '' }}">
            <div class="uk-width-1-1 uk-width-medium-1-2">
                <select class="{{ id == '-1' ? 'uk-hidden' : '' }} network__id" name="network[ids][]">
                    <option {{ id == '' ? 'selected' : '' }} value>-- Выбирете Соц. Сеть --</option>
                    {% for net in existNetworks %}
                        <option {{ id == net.id ? 'selected 1111111111' : '' }} value="{{ net.id }}">{{ net.name }}</option>
                    {% endfor %}
                    <option {{ id == '-1' ? 'selected' : '' }} value="-1">Другая</option>
                </select>
                <input type="text" value="{{ title | e }}" class="network__title {{ id == '-1' ? '' : 'uk-hidden' }}" name="network[titles][]" placeholder="Название соц. сети">
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-2">
                <input type="text" value="{{ link | e }}" class="network__link" name="network[links][]" placeholder="Ссылка на профиль">
            </div>
            <div class="{{ isNew ? 'uk-hidden' : '' }} uk-width-1-1 network-delete">
                <button class="uk-button-small uk-button-danger">Удалить соц. сеть</button>
            </div>
            {% for str in msg %}
                <div class="uk-width-1-1">
                    <div class="uk-alert uk-alert-danger uk-margin-small-top">
                        {{ str }}
                    </div>
                </div>
            {% endfor %}
        </div>
    {%- endmacro %}

    <div class="uk-margin-large">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
                <div class="uk-width-1-1">
                    <hr>
                </div>
                <div class="uk-width-1-1 uk-width-medium-1-2 uk-container-center">
                    <form method="POST" enctype="multipart/form-data" class="uk-width-1-1 uk-margin-large-top uk-form uk-form-horizontal">
                        {% for element in form %}
                            {% if element.getUserOption('loop') == 1 %}
                                <div class="uk-form-row">
                                    {% if element.getLabel() %}
                                        <div class="uk-form-label">
                                            {{ form.label(element.getName()) }}
                                            {% if element.getAttribute('required') or element.getUserOption('required') %}
                                                <sup class="uk-text-danger">*</sup>
                                            {% endif %}
                                        </div>
                                    {% endif %}

                                    <div class="uk-form-controls">
                                        {{ form.render(element.getName(), ['class': 'uk-width-1-1']) }}

                                        {% if form.messages(element.getName()) is not empty %}
                                            {% for msg in form.messages(element.getName()) %}
                                                <div class="uk-alert uk-alert-danger">
                                                    {{ msg }}
                                                </div>
                                            {% endfor  %}
                                        {% endif %}
                                    </div>
                                </div>
                            {% endif %}
                        {% endfor %}

                        <legend class="uk-margin-top networks-legend">Социальные сети</legend>
                        <div class="networks-content">
                            {{ printNetwork('', '', '', [], existNetworks, true, true) }}
                            {% for net in networks %}
                                {{ printNetwork(
                                        net['id'] is defined ? net['id'] : '',
                                        net['title'] is defined ? net['title'] : '',
                                        net['link'] is defined ? net['link'] : '',
                                        net['msg'],
                                        existNetworks,
                                        false,
                                        false
                                ) }}
                            {% endfor %}
                            {{ printNetwork('', '', '', [], existNetworks, true, false) }}
                        </div>

                        <div class="uk-width-1-1 uk-margin-top uk-text-right">
                            {{ form.render('send', ['class': "uk-button"]) }}
                            <a href="{{ ref }}" class="uk-button">
                                Отменить
                            </a>
                            {% if query['contactId'] is defined %}
                                <button type="button" class="uk-button uk-button-danger" data-uk-modal="{target:'#confirmDelete'}">
                                    Удалить
                                </button>
                            {% endif %}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {% if query['contactId'] is defined %}
        <div id="confirmDelete" class="uk-modal">
            <div class="uk-modal-dialog uk-text-left">
                <a href="" class="uk-modal-close uk-close"></a>
                <p class="uk-h2">Подтверждение удаления</p>
                <form method="POST">
                    <p>Вы действительно хотите удалить контакт?</p>
                    <div class="uk-text-right">
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="uk-button">
                            Да
                        </button>
                        <a class="uk-button uk-modal-close">
                            Нет
                        </a>
                    </div>
                </form>
            </div>
        </div>
    {% endif %}
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    {{ javascript_include('/js/contactForm.js') }}
{% endblock %}