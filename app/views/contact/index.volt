{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}

    <div class="uk-margin-large">
        <div class="uk-container uk-container-center container">
            <div class="uk-grid uk-mar">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
            </div>

            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <a href="/contact/form">
                        <i class="uk-icon-plus uk-icon-medium uk-display-inline"></i>&nbsp;Добавить запись
                    </a>
                </div>
                <div class="uk-width-1-1">
                    <hr>
                </div>
            </div>

            <div class="uk-grid contacts">
                {{ partial("partials/contactTable", ['contacts': contacts, 'query': query, 'data': data, 'url': '/contact/ajaxTable']) }}
            </div>

        </div>
    </div>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    {{ javascript_include('/js/contactList.js') }}
{% endblock %}