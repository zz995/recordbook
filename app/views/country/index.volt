{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}

    {%- macro printTableRow(country, isHead) %}
        <div class="uk-grid">
            <div class="uk-width-2-10 uk-width-medium-1-10">
                {{ country['n'] }}
            </div>
            <div class="uk-width-8-10 uk-width-medium-3-10">
                {{ country['name'] }}
            </div>
            <div class="uk-visible-small uk-margin-small-top"></div>
            <div class="uk-width-1-1 uk-width-medium-3-10">
                {% if isHead %}
                    {{ country['edit'] }}
                {% else %}
                    <a href="/country/form/{{ country['n'] }}">
                        {{ country['edit'] }}
                    </a>
                {% endif %}
            </div>
        </div>
    {%- endmacro %}

    <div class="uk-margin-large">
        <div class="uk-container uk-container-center container">
            <div class="uk-grid uk-mar">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
            </div>

            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <a href="/country/form">
                        <i class="uk-icon-plus uk-icon-medium uk-display-inline"></i>&nbsp;Добавить страну
                    </a>
                </div>
                <div class="uk-width-1-1">
                    <hr>
                </div>
            </div>

            <div class="uk-grid">
                {% if countries.count() is empty %}
                    <div class="uk-width-1-1">
                        Ничего не найдено
                    </div>
                {% else %}
                    <div class="uk-width-1-1 countries table">
                        <div class="table__head">
                            {{ printTableRow(['n': '№', 'name': 'Название', 'edit': ''], true) }}
                        </div>
                        {% for country in countries %}
                            <div class="table__main">
                                {{ printTableRow(['n': country.id, 'name': country.name, 'edit': 'Редактировать'], false) }}
                            </div>
                        {% endfor %}
                    </div>
                {% endif %}
            </div>

        </div>
    </div>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}