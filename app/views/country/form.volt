{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}
    <div class="uk-margin-large">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
                <div class="uk-width-1-1">
                    <hr>
                </div>
                <div class="uk-width-1-1 uk-width-medium-1-2 uk-container-center">
                    <form method="POST" enctype="multipart/form-data" class="uk-width-1-1 uk-margin-large-top uk-form uk-form-horizontal">
                        {% for element in form %}
                            {% if element.getUserOption('loop') == 1 %}
                                <div class="uk-form-row">
                                    {% if element.getLabel() %}
                                        <div class="uk-form-label">
                                            {{ form.label(element.getName()) }}
                                            {% if element.getAttribute('required') or element.getUserOption('required') %}
                                                <sup class="uk-text-danger">*</sup>
                                            {% endif %}
                                        </div>
                                    {% endif %}

                                    <div class="uk-form-controls">
                                        {{ form.render(element.getName(), ['class': 'uk-width-1-1']) }}

                                        {% if form.messages(element.getName()) is not empty %}
                                            {% for msg in form.messages(element.getName()) %}
                                                <div class="uk-alert uk-alert-danger">
                                                    {{ msg }}
                                                </div>
                                            {% endfor  %}
                                        {% endif %}
                                    </div>
                                </div>
                            {% endif %}
                        {% endfor %}

                        <div class="uk-width-1-1 uk-margin-top uk-text-right">
                            {{ form.render('send', ['class': "uk-button"]) }}
                            <a href="{{ ref }}" class="uk-button">
                                Отменить
                            </a>
                            {% if query['countryId'] is defined and data['countBindCities'] is 0 %}
                                <button type="button" class="uk-button uk-button-danger" data-uk-modal="{target:'#confirmDelete'}">
                                    Удалить
                                </button>
                            {% endif %}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {% if query['countryId'] is defined and data['countBindCities'] is 0 %}
        <div id="confirmDelete" class="uk-modal">
            <div class="uk-modal-dialog uk-text-left">
                <a href="" class="uk-modal-close uk-close"></a>
                <p class="uk-h2">Подтверждение удаления</p>
                <form method="POST">
                    <p>Вы действительно хотите удалить страну?</p>
                    <div class="uk-text-right">
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="uk-button">
                            Да
                        </button>
                        <a class="uk-button uk-modal-close">
                            Нет
                        </a>
                    </div>
                </form>
            </div>
        </div>
    {% endif %}
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}