<div class="uk-grid">
    <div class="uk-width-1-1 uk-width-medium-2-3">
        <p>
            <strong>ФИО:</strong>
            <span class="">
                {{ contact.surname }}
                {{ contact.name }}
                {{ contact.patronymic }}
            </span>
        </p>
        <p>
            <strong>Номер телефона:</strong>
            <span class="">
                {{ contact.phone }}
            </span>
        </p>
        <p>
            <strong>Страна рождения:</strong>
            <span class="">
                {{ contact.city.country.name }}
            </span>
        </p>
        <p>
            <strong>Город рождения:</strong>
            <span class="">
                {{ contact.city.name }}
            </span>
        </p>
        <p>
            <strong>Электронная почта:</strong>
            <span class="">
                {{ contact.email }}
            </span>
        </p>
        {% if contact.links | length > 0 %}
            <p>
                <strong>Социальные сети:</strong>
                <br>
                {% for link in contact.links %}
                    <a href="{{ link.url }}">{{ link.networks.name }}</a>{{ loop.last ? '' : ',' }}
                {% endfor %}
            </p>
        {% endif %}
    </div>
    <div class="uk-width-1-1 uk-width-medium-1-3">
        <img src="{{ contact.photo }}">
    </div>
</div>