<?php

namespace MyApp\Forms;

use MyApp\Models\City;
use MyApp\Models\Country;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\File as FileValidator;

class ContactForm extends FormBase
{
    public function initialize($entity = null, $options = null)
    {
        $surname = new Text('surname');
        $surname->setUserOption('loop', 1);
        $surname->setLabel('Фамилия');
        $surname->setFilters([
            'string',
            'trim'
        ]);
        $surname->setAttribute('required', 'true');
        $surname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Поле Фамилия обязательно для заполнения'
            ))
        ));
        $this->add($surname);

        $name = new Text('name');
        $name->setUserOption('loop', 1);
        $name->setLabel('Имя');
        $name->setFilters([
            'string',
            'trim'
        ]);
        $name->setAttribute('required', 'true');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Поле Имя обязательно для заполнения'
            ))
        ));
        $this->add($name);

        $patronymic = new Text('patronymic');
        $patronymic->setUserOption('loop', 1);
        $patronymic->setLabel('Отчество');
        $patronymic->setFilters([
            'string',
            'trim'
        ]);
        $patronymic->setAttribute('required', 'true');
        $patronymic->addValidators(array(
            new PresenceOf(array(
                'message' => 'Поле Отчество обязательно для заполнения'
            ))
        ));
        $this->add($patronymic);

        $birth = new Date('birth');
        $birth->setUserOption('loop', 1);
        $birth->addValidators(array(
            new PresenceOf(array(
                'message' => 'Поле Дата рождения обязательно для заполнения'
            ))
        ));
        $birth->setAttribute('required', 'true');
        $birth->setLabel('Дата рождения');
        $this->add($birth);

        $image = new File('image');
        $image->setUserOption('loop', 1);
        $image->addValidators(array(
            new FileValidator(array(
                'maxSize' => '2M',
                'messageSize' => 'Изображение превысило розмер в 2МВ',
                'allowedTypes' => array('image/jpeg', 'image/png'),
                'messageType' => 'Ваше изображение должно быть формата JPEG или PNG',
                'allowEmpty' => true,
            ))
        ));
        $image->setLabel('Загрузить изображение');
        $this->add($image);

        $phone = new Text('phone', ["id" => "formRegisterPhone", "placeholder" => "38XXXXXXXXXX"]);
        $phone->setUserOption('loop', 1);
        $phone->setLabel('Номер телефона');
        $phone->setAttribute('required', 'true');
        $phone->setFilters([
            'string',
            'trim'
        ]);
        $phone->addValidators(array(
            new Regex(array(
                "message"    => "Телефон введен не верно.<br>Пример: 38XXXXXXXXXX",
                "pattern"    => "/38[0-9]{10}/",
                //"allowEmpty" => true
            ))
        ));
        $this->add($phone);

        $email = new Text('email');
        $email->setUserOption('loop', 1);
        $email->setLabel('Электронная почта');
        $email->setFilters([
            'striptags',
            'trim'
        ]);
        $email->setAttribute('required', 'true');
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Поле Электронная почта обязательно для заполнения',
                "cancelOnFail" => true
            )),
            new Email(array(
                "message" => "Электронная почта неправильная",
                "cancelOnFail" => true
            ))
        ));
        $this->add($email);

        $country = new Select(
            'country_id',
            Country::find(),
            [
                'useEmpty' => true,
                'emptyText' => '-- Выберите страну --',
                'emptyValue' => null,
                "using" => [
                    "id",
                    "name",
                ]
            ]
        );
        $country->setUserOption('loop', 1);
        $country->setLabel('Страна');
        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Необходимо указать страну'
            ))
        ));
        $country->setAttribute('required', 'true');
        $this->add($country);

        $city = new Select('city_id',
            City::find(
                [
                    "country_id = :id:",
                    "bind" => [
                        "id" => @$options['country']
                    ]
                ]
            ),
            [
                'class' => 'field-large',
                'useEmpty' => true,
                'emptyText' => '-- Выбирете город --',
                'emptyValue' => null,
                "using" => [
                    "id",
                    "name",
                ]
            ]
        );
        if (!isset($options['country'])) {
            $city->setAttribute('disabled', 'true');
        }
        $city->setUserOption('loop', 1);
        $city->setLabel('Город');
        $city->addValidators(array(
            new PresenceOf(array(
                'message' => 'Необходимо указать город'
            ))
        ));
        $city->setAttribute('required', 'true');
        $this->add($city);

        $csrf = new Hidden('csrf');
        $csrf->setUserOption('loop', 1);
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        $submit = new Submit('send');
        $submit->setDefault('Отправить');
        $this->add($submit);
    }
}