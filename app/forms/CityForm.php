<?php

namespace MyApp\Forms;

use MyApp\Models\Country;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\File as FileValidator;

class CityForm extends FormBase
{
    public function initialize($entity = null, $options = null)
    {

        $name = new Text('name');
        $name->setUserOption('loop', 1);
        $name->setLabel('Название');
        $name->setFilters([
            'string',
            'trim'
        ]);
        $name->setAttribute('required', 'true');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Поле Название обязательно для заполнения'
            ))
        ));
        $this->add($name);

        $country = new Select(
            'country_id',
            Country::find(),
            [
                'class' => 'field-large',
                'useEmpty' => true,
                'emptyText' => '-- Выберете страну --',
                'emptyValue' => null,
                "using" => [
                    "id",
                    "name",
                ]
            ]
        );
        $country->setUserOption('loop', 1);
        $country->setLabel('Страна');
        $country->setAttribute('required', 'true');
        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Необходимо выбрать страну'
            ))
        ));
        $this->add($country);

        $submit = new Submit('send');
        $submit->setUserOption('loop', 0);
        $submit->setDefault('Отправить');
        $this->add($submit);
    }
}