<?php

namespace MyApp\Forms;

use Phalcon\Forms\Form;

class FormBase extends Form
{
    public function getFiles($name) {
        return array_filter($this->request->getUploadedFiles(), function ($file) use($name) {
            return preg_match("/^$name(\\[\\]$|$)/", $file->getKey()) && $file->getSize();
        });
    }

    public function validFiles()
    {
        $isValid = true;
        $elements = $this->getElements();
        foreach ($elements as $elem) {
            if (get_class($elem) != 'Phalcon\Forms\Element\File') continue;

            $validators = $elem->getValidators();
            foreach ($validators as $validator) {
                $name = $elem->getName();
                $validation = new \Phalcon\Validation();
                $validation->add($name, $validator);
                $curMessages = $validation->validate($_FILES);
                $messages = $this->getMessagesFor($name);
                foreach ($curMessages as $message) {
                    $isValid = false;
                    $messages->appendMessage($message);
                }
            }
        }

        return $isValid;
    }

    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            return $this->getMessagesFor($name);
        } else {
            return null;
        }
    }
}