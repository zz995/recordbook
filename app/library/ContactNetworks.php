<?php

namespace MyApp\Library;

use MyApp\Models\Contact;
use MyApp\Models\Link;
use MyApp\Models\Network;

class ContactNetworks {

    protected $networks = [];
    protected $allowIds;

    public function __construct($networks) {
        $this->allowIds = [];
        foreach ($networks as $net) {
            $this->allowIds[] = $net->id;
        }
    }

    public function getNetworks() {
        return $this->networks;
    }

    public function setContactNetworks(Contact $contact) {
        if (isset($contact->links)) {
            foreach ($contact->links as $link) {
                $this->networks[] = ['id' => $link->network_id, 'title' => null, 'link' => $link->url, 'msg' => []];
            }
        } else {
            $this->networks = [];
        }
    }

    public function setNetworks($networks) {

        if (is_array($networks) && is_array(@$networks['ids'])) {
            $ids = array_slice($networks['ids'], 1, -1);
            $titles = @$networks['titles'];
            $links = @$networks['links'];
            if (!is_array($titles)) {
                $titles = [];
            }
            if (!is_array($links)) {
                $links = [];
            }
            $titles = array_slice($titles, 1, -1);
            $links = array_slice($links, 1, -1);

            foreach ($ids as $key => $id) {
                $title = $titles[$key];
                $link = $links[$key];

                if (empty($id)) {
                    $id = null;
                } else {
                    $id = intval($id);
                }

                $title = trim(strip_tags($title ?: ''));
                if (empty($title)) {
                    $title = null;
                }

                $link = trim(strip_tags($link ?: ''));
                if (empty($link)) {
                    $link = null;
                }

                $this->networks[] = ['id' => $id, 'title' => $title, 'link' => $link, 'msg' => []];
            }
        } else {
            $this->networks = [];
        }
    }

    public function isValid() {
        $networks = $this->networks;
        $allowIds = $this->allowIds;

        $valid = true;
        foreach ($networks as $key => $net) {
            $id = $net['id'];
            $title = $net['title'];
            $link = $net['link'];
            $this->networks[$key]['msg'] = [];

            if (is_null($id) || ($id == -1 && is_null($title)) || ($id != -1 && is_null($id) && !in_array($id, $allowIds))) {
                $valid = false;
                $this->networks[$key]['msg'][] = 'Социальная сеть не указана';
            }

            if (is_null($link)) {
                $valid = false;
                $this->networks[$key]['msg'][] = 'Ссылка на профиль не указана';
            }
        }

        return $valid;
    }

    public function save(Contact $contact) {
        foreach ($this->networks as $key => $net) {
            if ($net['id'] != -1)  continue;

            $newNet = new Network();
            $newNet->name = $net['title'];
            if ($newNet->save()) {
                $this->networks[$key]['id'] = $newNet->id;
            } else {
                throw new \Exception('Неудалось сохранить социальную сеть');
            }
        }

        $contactId = $contact->id;
        Link::find("contact_id = $contactId")->delete();
        foreach ($this->networks as $key => $net) {
            if (is_null($net['id']) || $net['id'] == -1) continue;

            $newLink = new Link();
            $newLink->url = $net['link'];
            $newLink->network_id = $net['id'];
            $newLink->contact_id = $contactId;

            if ($newLink->save()) {
                $this->networks[$key]['id'] = $newLink->id;
            } else {
                throw new \Exception('Неудалось сохранить социальную сеть');
            }
        }

        return true;
    }
}